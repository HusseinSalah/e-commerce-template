/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  View,
  Text,
  StatusBar,
  I18nManager
} from 'react-native';
import language from '../src/language';
import Home from '../src/navigation/ChatStack';
import Register from '../src/containers/AuthScreens/Register';
// import ContactChat from '../src/containers/chat/ContactChat';
import Localization from '../src/language';

Localization.setLanguage('ar');
I18nManager.forceRTL(true);
const App = () => {
  return (
    <Home />
  );
};


export default App;
