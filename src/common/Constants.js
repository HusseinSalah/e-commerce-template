import {
    Dimensions,
} from 'react-native';

export const {width, height} = Dimensions.get('window');
export const CONSTANTS = {
    numColumns: 3,
    drawerWidth: width*0.75,
    
}