import Localization from '../language';


const validateFirstName = (firstName)=>{
    console.log("firstName: " + firstName);
    return firstName.length==0?Localization.firstNameErrorMsg:null;
}

const validateLastName = (lastName)=>{
    console.log("lastName: " + lastName);
    return lastName.length==0?Localization.lastNameErrorMsg:null;
}

const validateEmail = (email)=>{
    console.log("email: " + email);
    let mailReg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    return !mailReg.test(String(email).toLowerCase())?Localization.emailErrorMsg:null;
}

const validatePhone = (phone)=>{
    console.log("phone: " + phone);
    return !isNaN(phone) && phone.length >= 10 ?null:Localization.phoneErrorMsg;
}

const validatePasswordAndConfirm = (password, passwordConfirm)=>{
    console.log("password: " + password + "passwordConfirm: " + passwordConfirm);
    return password != passwordConfirm ? Localization.passwordDontMatchErrorMsg:null;
}

const validatePassword = (password)=>{
    console.log("password: " + password);
    return password.length<6?Localization.passwordErrorMsg:null;
}

const validatePasswordConfirm = (passwordConfirm)=>{
    console.log("passwordConfirm: " + passwordConfirm);
    return passwordConfirm.length<6?Localization.passwordConfirmErrorMsg:null;
}

export {
    validateFirstName,
    validateLastName,
    validateEmail,
    validatePhone,
    validatePassword,
    validatePasswordAndConfirm,
    validatePasswordConfirm,
}