import React from 'react';
import {
    FlatList,
    Text,
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    Image,
    I18nManager
} from 'react-native';
import {width, height, COLORS, IMAGES, CONSTANTS, ICONS} from '../../common';
import { HeaderText } from '../HeaderText';
import Localization from '../../language';

export default class CustomFlatList extends React.Component {
    _renderItem = ({item})=>{
        return(
            <TouchableWithoutFeedback onPress={this.props.onPress}>
                <View style={styles.productCard}>
                    <Image source={item.image} style={styles.productImage}/>
                    <View style={[styles.float ,
                            !I18nManager.isRTL?
                            {
                                borderTopRightRadius:width*0.02,
                                borderBottomRightRadius:width*0.02,
                            }:
                            {
                                borderTopLeftRadius:width*0.02,
                                borderBottomLeftRadius:width*0.02,
                            }
                            ]}>
                        <Text style={styles.productText}> {item.name} </Text>
                        <View style={styles.productOption}>
                            <Text style={styles.priceText}> 50$ </Text>
                            <View style={styles.addCartText}>
                                <Text style={styles.cartText}> {Localization.addToCart} </Text>
                                <Image source={IMAGES.cartIcon} style={styles.icon}/>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }
    render(){
        const {
            title,
            bannerHeight,
            items,
        } = this.props;
        // console.warn(JSON.stringify(bannerHeight))
        return(
            <View>
                <HeaderText title={title}/>
                <FlatList
                    ref="list"
                    extraData={this.state}
                    keyExtractor={(item, index) => `${index}`}
                    data={items}
                    renderItem={this._renderItem}
                    // horizontal
                    pagingEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    numColumns={CONSTANTS.numColumns}
                    // onMomentumScrollEnd={this.onScrollEnd}
                    // contentContainerStyle={[I18nManager.isRTL && styles.rtlList]} 
                    // style={{height:height*(bannerHeight || 0.2)}} 
                    />
            </View>
            // <Text> lol </Text>
        )
    }
}

let cardWidth = width * 1/(CONSTANTS.numColumns) - width*0.01*CONSTANTS.numColumns;

const styles = StyleSheet.create({
    productCard:{
        width:cardWidth,
        height:cardWidth + height*(0.16/CONSTANTS.numColumns),
        borderWidth: width*0.005,
        borderRadius: width*0.02,
        margin: width*0.01,
        borderColor:COLORS.main,
        elevation:5
    },
    productImage:{
        resizeMode:'cover',
        width:cardWidth - width*0.01,
        height: cardWidth - width*0.01,
        borderRadius: width*0.02,
    },
    float:{
        flex:1,
        backgroundColor:COLORS.background,
        justifyContent:'space-between', 
        alignItems:'center',
        
        // position:'absolute',
        // left: width*0.01,
        // bottom: width*0.01,
    },
    productText:{
        color:COLORS.black,
        fontSize:width*0.03*(3/CONSTANTS.numColumns),
        textAlign:'center',
        textAlignVertical:'center',
        width:cardWidth,
    },
    icon:{
        width:width*0.03*(3/CONSTANTS.numColumns),
        height:width*0.03*(3/CONSTANTS.numColumns),
        resizeMode:'contain',
        marginHorizontal:width*0.01
    },
    productOption:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        alignSelf:'baseline',
    },
    priceText:{
        backgroundColor:COLORS.main,
        fontSize:width*0.03*(3/CONSTANTS.numColumns),
        fontWeight:'bold',
        width:cardWidth*0.25 - width*0.01,
        textAlign:'center',
        textAlignVertical:'center'
    },
    addCartText:{
        width:cardWidth*0.75,
        justifyContent:'flex-end',
        alignItems:'center',
        flexDirection:'row',
    },
    cartText:{
        color:COLORS.black,
        fontSize:width*0.028*(3/CONSTANTS.numColumns),
        textAlign:'center',
        textAlignVertical:'center',
    }
})