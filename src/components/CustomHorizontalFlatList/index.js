import React from 'react';
import {
    FlatList,
    Text,
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    Image,
} from 'react-native';
import {width, height, COLORS, IMAGES, CONSTANTS} from '../../common';
import {HeaderText} from '../HeaderText'; 
export default class CustomHorizontalFlatList extends React.Component {
    _renderItem = ({item})=>{
        return(
            <TouchableWithoutFeedback onPress={this.props.onPress}>
                <View style={styles.itemContainer}>
                    <Image source={item.image} style={styles.categoryImage}/>
                </View>
            </TouchableWithoutFeedback>
        )
    }
    render(){
        const {
            title,
            bannerHeight,
            items,
        } = this.props;
        // console.warn(JSON.stringify(bannerHeight))
        return(
            <View style={styles.topCategoriesContainer}>
                <HeaderText title={title||""}/>
                <FlatList
                    ref="list"
                    extraData={this.state}
                    keyExtractor={(item, index) => `${index}`}
                    data={items}
                    renderItem={this._renderItem}
                    horizontal
                    pagingEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    style={{height:height*0.25}}
                    />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    topCategoriesContainer:{
        height: height*0.2,
        padding: width*0.01
    },
    itemContainer:{
        width:width*0.2,
        height:width*0.2,
        borderRadius:width*0.02,
        marginHorizontal:width*0.02,
        elevation:5
    },
    categoryImage:{
        borderWidth:width*0.005,
        borderColor:COLORS.main,
        width:width*0.2,
        height:width*0.2,
        borderRadius:width*0.02,
        resizeMode:'cover',
    }
})