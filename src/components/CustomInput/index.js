import React from 'react'

import {
    Image,
    StyleSheet,
    View,
    Text,
    I18nManager,
    TextInput
} from 'react-native';
import {IMAGES, COLORS} from '../../common';
import {width, height} from '../../common';
import { Icon } from "react-native-elements";
export default class CustomInput extends React.Component {
    constructor(){
        super();
        this.state = {
            error:true,
        }
    }
    render(){
        return (
            <View style={{minHeight:height*0.1}}>
                {/* {this.state.focused?<Text style={styles.inputHeader}> {this.props.placeholder} </Text>:null} */}
                <View style={styles.textInputCotainer}>
                    <View>
                        <Icon name={this.props.iconName} type='font-awesome' size={width*0.05} color={COLORS.main} />                
                    </View>
                    <TextInput
                        placeholder={this.props.placeholder}
                        style={styles.textInput}
                        // onFocus={()=>this.setState({focused:true})}
                        onSubmitEditing={this.props.onSubmitEditing}
                        placeholderTextColor={COLORS.textGray}
                        secureTextEntry={this.props.password}
                        onChangeText={this.props.onChangeText}
                        returnKeyType={this.props.returnKeyType}
                        returnKeyLabel={this.props.returnKeyLabel}
                        keyboardType={this.props.keyboardType}
                        blurOnSubmit={this.props.returnKeyLabel=="go"}
                        ref={this.props.reference}
                        />
                </View>
                {this.props.error?<Text style={styles.inputError}> {this.props.error} </Text>:null}
            </View>

        )
    }
}

const styles = StyleSheet.create({
    textInputCotainer:{
        justifyContent:'space-between',
        alignItems:'center',
        flexDirection:'row',
        borderRadius:width*0.04,
        // borderWidth:width*0.002,
        // borderColor:COLORS.main,
        width:width*0.8,
        height: height*0.06,
        backgroundColor:COLORS.text,
        padding:width*0.02,
        elevation: 3,
        // marginTop:height*0.02,
    },
    textInput:{
        padding:0,
        textAlign:!I18nManager.isRTL?'left':'right',
        textAlignVertical:'center',
        width:width*0.68,
        fontSize:width*0.035,
        color:COLORS.textBlack
    },
    inputHeader:{
        fontSize:width*0.03,
        color:COLORS.textBlack,
        marginBottom:width*0.02
    },
    inputError:{
        fontSize:width*0.03,
        color:COLORS.secondery,
        marginTop:width*0.02,
        marginHorizontal:width*0.02
    }
});
