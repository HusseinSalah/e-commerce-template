import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
} from 'react-native';
import { IMAGES, COLORS, width, height, CONSTANTS } from '../../common';
import { Header} from '../../components';
import { FlatList } from 'react-native-gesture-handler';
import { ICONS } from 'jest-util/build/specialChars';
import Localization from '../../language';
class Drawer extends React.Component {
    constructor(){
        super();
        this.state = {
            drawerLinks:[
                {id:0,name:Localization.home, icon:IMAGES.home, link:'Home'},
                {id:1,name:Localization.profile, icon:IMAGES.profile, link:'Profile'},
                {id:2,name:Localization.cart, icon:IMAGES.orders, link:'Cart'},
                {id:3,name:Localization.myOrders, icon:IMAGES.orders, link:'Settings'},
                {id:4,name:Localization.settings, icon:IMAGES.settings, link:'Settings'},
                {id:5,name:Localization.callus, icon:IMAGES.callus, link:'Callus'},
                {id:6,name:Localization.terms, icon:IMAGES.terms, link:'Terms'},
                {id:7,name:Localization.logout, icon:IMAGES.logout, link:'logout'},
            ],
            activePage:0,
        }
    }
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image source={IMAGES.product2} style={styles.imageProfile}/>
                    <View style={{paddingHorizontal:width*0.03}}>
                        <Text style={{color:COLORS.background, fontSize:width*0.035, margin:width*0.004, fontWeight:'bold'}}> Arab Team E-Commerce </Text> 
                        {/* <Text style={{color:COLORS.background, fontSize:width*0.035, margin:width*0.004, fontWeight:'bold'}}> +02 01023239809 </Text>  */}
                    </View>
                    {/* <Text style={{position:'absolute', color:COLORS.background, fontSize:width*0.035, bottom:0, right:0}}> logout </Text> */}
                </View>
                <View style={styles.links}>
                    <FlatList
                        data={this.state.drawerLinks}
                        renderItem = {({item})=>{
                            return(
                                <TouchableOpacity style={[styles.drawerItem, this.state.activePage==item.id?{backgroundColor:"rgba(200,200,200,0.5)"}:null]} onPress={()=>{this.setState({activePage:item.id});this.props.navigation.navigate(item.link)}}>
                                    <Image source={IMAGES.cartIcon} style={styles.icon}/>
                                    <Text style={styles.text}> {item.name} </Text>
                                </TouchableOpacity>
                            )
                        }}
                    />
                </View>
            </View>
        )
    }
}

export default Drawer;

styles = StyleSheet.create({
    container:{
        height:height,
        width:"100%",
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:COLORS.background,
    },
    header:{
        height:height*0.15,
        width:"100%",
        // flex:2,
        justifyContent:'flex-start',
        alignItems:'center',
        backgroundColor: COLORS.main,
        flexDirection:'row',
        
    },
    links:{
        width:"100%",
        height:height*0.85,

    },
    text:{
        color :'black',
        fontSize: width*0.05,
    },
    drawerItem:{
        height:height*0.09,
        justifyContent:'flex-start',
        alignItems:'center',
        flexDirection:'row',
    },
    imageProfile:{
        width:width*0.18,
        height:width*0.18,
        resizeMode:'contain',
        borderRadius: width*0.15,
        margin:width*0.03
    },
    icon:{
        width:width*0.05,
        height:width*0.05,
        resizeMode:'contain',
        margin:width*0.04    
    }
}) 