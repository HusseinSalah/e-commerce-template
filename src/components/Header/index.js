import React from 'react'

import {
    Image,
    StyleSheet,
    View,
    Text,
    I18nManager
} from 'react-native';
import {IMAGES, COLORS} from '../../common';
import {width, height} from '../../common';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {withNavigation} from 'react-navigation';
class Header extends React.Component {
    render(){
        return (
            <View style={styles.header}>
                <TouchableOpacity onPress={()=>this.props.navigation.toggleDrawer()}>
                <Image source={IMAGES.menuIcon} style={styles.image6_5}/>
                </TouchableOpacity>
                <View style={styles.rowCenter}>
                    <Text style={styles.textHeader}> {this.props.title} </Text>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack(null)}>
                    <Image source={IMAGES.backIcon} style={[styles.image4_5, !I18nManager.isRTL?{transform: [{ rotate: '180deg' }]}:null]}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
export default withNavigation(Header);
// export default Header;
wp = (w)=>{
    return (parseFloat(w)/100)*width;
}
hp = (h)=>{
    return (parseFloat(h)/100)*height;
}

const styles = StyleSheet.create({
    header:{
        height:hp('8%'),
        backgroundColor:COLORS.main,
        justifyContent:'space-between',
        alignItems:'center',
        padding:wp('5%'),
        flexDirection:'row'
    },
    image4_5:{
        width:wp('4.5%'), 
        height:wp('4.5%'), 
        resizeMode:'contain',
    },
    rowCenter:{
        justifyContent:'center', 
        alignItems:'center', 
        flexDirection:'row'
    },
    textHeader:{
        color:'white', 
        fontSize:wp('4.5%'), 
        marginHorizontal:wp('3%'), 
        fontWeight:'bold',
        textAlign:'left',
        width:width*0.72
    },
    image6_5:{
        width:wp('6.5%'), 
        height:wp('6.5%'), 
        resizeMode:'contain'
    },
});
