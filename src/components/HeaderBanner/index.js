import React from 'react';
import {
    FlatList,
    Text,
    View,
    StyleSheet,
    TouchableWithoutFeedback,
    Image,
} from 'react-native';
import {width, height, COLORS, IMAGES} from '../../common';
export default class HeaderBanner extends React.Component {
    _renderItem = ({item})=>{
        return(
            <TouchableWithoutFeedback onPress={()=>console.warn(item.data)}>
                <View style={styles.bannerItem}>
                    <Image source={item.image} style={styles.bannerImage}/>
                    <View style={styles.float}>
                        <Text style={styles.textBanner}> {item.data} </Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }
    render(){
        const {
            bannerHeight,
            items,
        } = this.props;
        console.warn(JSON.stringify(bannerHeight))
        return(
            <View style={{height:height*(bannerHeight || 0.2)}}>
            <FlatList
                ref="list"
                extraData={this.state}
                keyExtractor={(item, index) => `${index}`}
                data={items}
                renderItem={this._renderItem}
                horizontal={true}
                pagingEnabled={true}
                showsHorizontalScrollIndicator={false}
                // onMomentumScrollEnd={this.onScrollEnd}
                // contentContainerStyle={[I18nManager.isRTL && styles.rtlList]} 
                style={{height:height*(bannerHeight || 0.2)}} 
                />
            </View>
            // <Text> lol </Text>
        )
    }
}

const styles = StyleSheet.create({
    bannerItem:{
        width:width,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:COLORS.background
    },
    bannerImage:{
        flex:1,
        resizeMode:'contain',
    },
    float:{
        flex:1,
        position:'absolute',
        left: width*0.01,
        bottom: width*0.01,
    },
    textBanner:{
        color:COLORS.text,
        fontSize:width*0.045
    }
})