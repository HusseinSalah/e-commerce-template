import React from 'react';
import { Text, StyleSheet, I18nManager } from 'react-native';
import { width, COLORS } from '../../common';


const HeaderText = ({ title, style, onPress, ...rest }) => (
    <Text style={[
        styles.text,
        style,
        !I18nManager.isRTL?
        {
            borderTopRightRadius:width*0.015,
            borderBottomRightRadius:width*0.015,
        }:
        {
            borderTopLeftRadius:width*0.015,
            borderBottomLeftRadius:width*0.015,
        }
    ]} onPress={onPress} >{title}</Text>
);
export { HeaderText };


const styles = StyleSheet.create({
    text: {
        marginVertical:width*0.02,
        fontSize:width*0.035,
        fontWeight:'bold',
        color:COLORS.textBlack,
        backgroundColor:COLORS.main,
        alignSelf:'baseline',
        padding:width*0.01,
        elevation:3
    }
})