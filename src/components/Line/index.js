import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import { COLORS, height as h, width as w } from '../../common';
const Line = ({width, height, color})=>{
    return(
        <View style={{height:height||h*0.002, backgroundColor:color||COLORS.background, width:width || w* 0.8}}/>
    );
}

export {Line};