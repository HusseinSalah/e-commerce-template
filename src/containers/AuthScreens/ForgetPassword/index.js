import React, { Component } from 'react';
import { 
  View, 
  Text,
  TouchableOpacity, 
  Image
} from 'react-native';
import {CheckBox} from 'react-native-elements';
import styles from './styles';
import CustomInput from '../../../components/CustomInput'; 
import Localization from '../../../language';
import { validatePassword, validateEmail, COLORS, width, height, IMAGES } from '../../../common';

class ForgetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      email:'',
      // password:'',
      emailError: '',
      // passwordError: '',
      // rememberMe:false,
    };
  }

  static navigationOptions = {
    header: null
  }

  _validate(){
    this.setState({error:null})
    let emailErr = validateEmail(this.state.email);
    // let passwordErr = validatePassword(this.state.password);
    this.setState({
      // passwordError: passwordErr,
      emailError: emailErr,
    })
    return emailErr || passwordErr;
  }
  
  submitLogin(){
    // console.warn("error")
    if(this._validate())return;
    console.warn("working?")
    // this.props.navigation.navigate('Home')
  }

  render() {
    return (
      <View style={styles.container}>

          <Image source={IMAGES.loginBG} style={styles.loginBG}/>

          <Text style={styles.logoText}>ARAB TEAM E-COMMERCE</Text>

          <CustomInput 
            placeholder={Localization.email}
            error={this.state.emailError}
            onChangeText={(email)=>this.setState({email})}
            reference={ref => this.emailRef = ref}
            onSubmitEditing={()=>this.passwordRef.focus()}
            returnKeyType="go"
            keyboardType="email-address"
            iconName="envelope"
            returnKeyLabel="go"
            />
          {/* <CustomInput 
            placeholder={Localization.password}
            error={this.state.passwordError}
            onChangeText={(password)=>this.setState({password})}
            password
            reference={ref => this.passwordRef = ref}
            returnKeyType="go"
            returnKeyLabel="go"
            iconName="lock"
          /> */}

          <TouchableOpacity onPress={()=>{this.submitLogin();}} style={styles.loginButton}>
              <Text style={styles.buttonText}> {Localization.submit} </Text>
          </TouchableOpacity>

          {/* <View style={{justifyContent:'space-between', width:width*0.75, marginVertical:height*0.01, flexDirection:'row'}}>
            <CheckBox size={width*0.05} textStyle={styles.text} uncheckedColor={COLORS.textBlack} checkedColor={COLORS.textBlack} onPress={()=>this.setState({rememberMe:!this.state.rememberMe})} containerStyle={styles.default} checked={this.state.rememberMe} title={Localization.rememberMe}/>
            <Text onPress={()=>this.props.navigation.navigate('ForgetPassword')} style={styles.text}> {Localization.forgetPassword} </Text>
          </View> */}

          {/* <View style={{flexDirection:'row', marginTop:width*0.05}}>
            <Text style={styles.text}> {Localization.createNewAccount} </Text>
            <Text onPress={()=>this.props.navigation.navigate('Register')} style={[styles.text, {color:COLORS.secondery, textDecorationLine:'underline'}]}>{Localization.createOne}</Text>
          </View> */}
      </View>
    );
  }
}
export default ForgetPassword