import {StyleSheet} from 'react-native'
import {width, height, COLORS} from '../../../common';

const styles = StyleSheet.create({
    container: {
        height:height,
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal:width*0.1,
        backgroundColor:COLORS.text,
        // paddingBottom:height*0.42
    },
    logoText:{
        // color: COLORS.textBlack,
        color:"rgba(80,80,80,1)",
        marginBottom: width*0.1,
        fontSize: width*0.08,
        fontWeight:'bold',
        width: width*0.8,
        textAlign:'center',
        textAlignVertical:'center',
        // marginTop:height*0.2,
    },
    loginButton:{
        width:width*0.8,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:width*0.05,
        backgroundColor:COLORS.main,
        height:height*0.06,
        marginHorizontal:width*0.03,
        elevation: 3
    },
    buttonText:{
        fontSize:width*0.05, 
        color:COLORS.text, 
        fontWeight:'bold'
    },
    default:{
        padding:0,
        margin:0,
        borderWidth:0,
        borderColor:COLORS.text,
        backgroundColor:"transparent",
    },
    text:{
        color:COLORS.textBlack,
        fontWeight:'bold',
        textAlign:'center',
        textAlignVertical:'center'
    },
    loginBG:{
        position:'absolute',
        height:height,
        width:width,
        resizeMode:'stretch', 
        opacity:0.6

    }
})

export default styles;