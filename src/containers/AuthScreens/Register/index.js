import React, { Component } from 'react';
import { 
  View, 
  Text,
  TouchableOpacity, 
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Platform
} from 'react-native';
import {CheckBox} from 'react-native-elements';
import styles from './styles';
import CustomInput from '../../../components/CustomInput'; 
import Localization from '../../../language';
import { validateFirstName, validateLastName, validateEmail, validatePhone, validatePassword, validatePasswordAndConfirm, validatePasswordConfirm, COLORS, width, height, IMAGES } from '../../../common';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      firstName:'',
      lastName:'',
      email:'',
      phone:'',
      password:'',
      passwordConfirm:'',
      firstNameError:'',
      lastNameError:'',
      emailError: '',
      phoneError:'',
      passwordError: '',
      passwordConfirmError: '',
      rememberMe:false,
    };
  }

  _validate(){
    this.setState({error:null})
    let firstNameErr = validateFirstName(this.state.firstName);
    let lastNameErr = validateLastName(this.state.lastName);
    let emailErr = validateEmail(this.state.email);
    let phoneErr = validatePhone(this.state.phone);
    let passwordErr = validatePassword(this.state.password);
    let passwordandCofirmErr = validatePasswordAndConfirm(this.state.password, this.state.passwordConfirm);
    let passwordConfirmErr = validatePasswordConfirm(this.state.passwordConfirm);
    this.setState({
      firstNameError: firstNameErr,
      lastNameError: lastNameErr,
      emailError: emailErr,
      phoneError: phoneErr,
      passwordError: passwordErr || passwordandCofirmErr,
      passwordConfirmError: passwordConfirmErr,
    })
    return firstNameErr || lastNameErr || emailErr || phoneErr || passwordErr || passwordandCofirmErr || passwordConfirmErr;
  }
  
  submitLogin(){
    // console.warn("error")
    if(this._validate())return;
    console.warn("working?")
    // this.props.navigation.navigate('Home')
  }

  render() {
    // const keyboardVerticalOffset = Platform.OS === 'ios' ? 40 : 0

    return (
      <KeyboardAvoidingView  
        behavior={Platform.OS === "ios" ? "padding" : null}
        keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}>
      <ScrollView>
      <View style={styles.container}>

          {/* <Image source={IMAGES.loginBG} style={styles.loginBG}/> */}

          <Text style={styles.logoText}>ARAB TEAM E-COMMERCE</Text>

          <CustomInput 
            placeholder={Localization.firstName}
            error={this.state.firstNameError}
            onChangeText={(firstName)=>this.setState({firstName})}
            reference={ref => this.firstNameRef = ref}
            onSubmitEditing={()=>this.lastNameRef.focus()}
            returnKeyType="next"
            iconName="user"
            />
          <CustomInput 
            placeholder={Localization.lastName}
            error={this.state.lastNameError}
            onChangeText={(lastName)=>this.setState({lastName})}
            reference={ref => this.lastNameRef = ref}
            onSubmitEditing={()=>this.emailRef.focus()}
            returnKeyType="next"
            iconName="user"
            />
          <CustomInput 
            placeholder={Localization.email}
            error={this.state.emailError}
            onChangeText={(email)=>this.setState({email})}
            reference={ref => this.emailRef = ref}
            onSubmitEditing={()=>this.phoneRef.focus()}
            returnKeyType="next"
            keyboardType="email-address"
            iconName="envelope"
            />
          <CustomInput 
            placeholder={Localization.phone}
            error={this.state.phoneError}
            onChangeText={(phone)=>this.setState({phone})}
            reference={ref => this.phoneRef = ref}
            onSubmitEditing={()=>this.passwordRef.focus()}
            returnKeyType="next"
            numeric
            iconName="phone"
            />
          <CustomInput 
            placeholder={Localization.password}
            error={this.state.passwordError}
            onChangeText={(password)=>this.setState({password})}
            password
            reference={ref => this.passwordRef = ref}
            onSubmitEditing={()=>this.passwordConfirmRef.focus()}
            returnKeyType="next"
            iconName="lock"
          />
          <CustomInput 
            placeholder={Localization.passwordConfirm}
            error={this.state.passwordConfirmError}
            onChangeText={(passwordConfirm)=>this.setState({passwordConfirm})}
            password
            reference={ref => this.passwordConfirmRef = ref}
            returnKeyType="go"
            returnKeyLabel="go"
            iconName="lock"
          />

          <TouchableOpacity onPress={()=>{this.submitLogin();}} style={styles.loginButton}>
              <Text style={styles.buttonText}> {Localization.register} </Text>
          </TouchableOpacity>

          {/* <View style={{justifyContent:'space-between', width:width*0.75, marginVertical:height*0.01, flexDirection:'row'}}>
            <CheckBox size={width*0.05} textStyle={styles.text} uncheckedColor={COLORS.textBlack} checkedColor={COLORS.textBlack} onPress={()=>this.setState({rememberMe:!this.state.rememberMe})} containerStyle={styles.default} checked={this.state.rememberMe} title={Localization.rememberMe}/>
            <Text onPress={()=>this.props.navigation.navigate('ForgetPassword')} style={styles.text}> {Localization.forgetPassword} </Text>
          </View> */}

          {/* <View style={{flexDirection:'row', marginTop:width*0.05}}>
            <Text style={styles.text}> {Localization.createNewAccount} </Text>
            <Text onPress={()=>this.props.navigation.navigate('Register')} style={[styles.text, {color:COLORS.secondery, textDecorationLine:'underline'}]}>{Localization.createOne}</Text>
          </View> */}
      </View>
      </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
export default Login