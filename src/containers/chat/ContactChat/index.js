import React from 'react';
import {
    View,
    Text,
    TextInput,
    FlatList,
    TouchableOpacity,
    Image,
} from 'react-native';
import {styles} from './styles';
import {width, height, COLORS, IMAGES, ICONS} from '../../../common';
import Localization from '../../../language';
import Header from '../../../components/Header';

class ContactChat extends React.Component{
    constructor(props){
        super();
        this.state={
            messeges:[
                {id:'1',from:'hussein salah', to:'Ghada Ahmed', messege:'السلام عليكم', date:'13:36'},
                {id:'2',from:'hussein salah', to:'Ghada Ahmed', messege:'كيف حالك؟', date:'13:36'},
                {id:'3',from:'Ghada Ahmed', to:'hussein salah', messege:'وعليكم السلام ورحمة الله وبركتة انا بخير وانت؟', date:'13:36'},
                {id:'4',from:'hussein salah', to:'Ghada Ahmed', messege:'بخير', date:'13:36'},
                {id:'5',from:'hussein salah', to:'Ghada Ahmed', messege:'كنت محتاج بس اعرف ايه اخر اخبار العروض عندكو؟', date:'13:36'},
                {id:'6',from:'hussein salah', to:'Ghada Ahmed', messege:'عروض السيارت', date:'13:36'},
                {id:'7',from:'Ghada Ahmed', to:'hussein salah', messege:'ان شاء الله قريب في عروض جديدة نازلة', date:'13:36'},
                {id:'8',from:'Ghada Ahmed', to:'hussein salah', messege:'اول اما نعلن عنها وننزلها هبلغ حضرتك يا فندم', date:'13:36'},
                {id:'9',from:'hussein salah', to:'Ghada Ahmed', messege:'طيب تمام جدا, شكرا لحضرتك.', date:'13:36'},
                {id:'10',from:'Ghada Ahmed', to:'hussein salah', messege:'العفو.', date:'13:36'},
            ],
            accountOwner:"hussein salah"

        }
    }

    
    static navigationOptions = ({navigation})=>{
        return({
            header: <Header title={navigation.getParam('OtherName', "LOOL")}/>
        })
    }

    _renderMessegeItem=({item})=>{
        return(
            <View style={styles.messegeItemContainer}>
                <View style={item.from==this.state.accountOwner?styles.myMessegeContainer:styles.otherMessegeContainer}> 
                    <Text style={item.from==this.state.accountOwner?styles.myMessege:styles.otherMessege}>{item.messege} </Text>    
                    <Text style={styles.date}> {item.date} </Text>
                </View>

            </View>
        )
    }

    render(){
        return(
            <View style={styles.container}>
                <Image source={IMAGES.chatBG} style={{resizeMode:'stretch', zIndex:-1, height:height, width:width, opacity:0.8, position:'absolute'}}/>
                <FlatList
                    data={this.state.messeges}
                    renderItem={this._renderMessegeItem}
                    keyExtractor={(item)=>item.id}
                />
                
                <View style={styles.messegeContainer}>
                    <TextInput 
                        placeholder={Localization.typeMessege}
                        style={styles.messegeInput}
                        multiline={true}
                    />
                    <TouchableOpacity>
                        <Text style={styles.send}> {Localization.send} </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default ContactChat;