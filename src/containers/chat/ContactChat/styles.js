import{
    StyleSheet
} from 'react-native';
import { width, height, COLORS } from '../../../common';

export const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',

    },
    textInput:{
        width:wp('60%'),
        color:'black',
        textAlign:'center',
        textAlignVertical:'center',
        fontSize:20,
        borderTopLeftRadius:30,
        borderBottomLeftRadius:30,
    },
    messegeContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'flex-end',
        width:width*0.96,
        marginVertical:width*0.02
    },
    messegeInput:{
        width:width*0.80,
        paddingHorizontal:width*0.03,
        fontSize:width*0.035,
        color:COLORS.textBlack,
        textAlignVertical:'center',
        backgroundColor:COLORS.background,
        minHeight: height*0.05,
        maxHeight: height*0.12,
        borderRadius:width*0.02,
        // borderTopLeftRadius:width*0.02,
        // borderBottomLeftRadius:width*0.02,
        paddingVertical:0,
    },
    send:{
        color:COLORS.text,
        backgroundColor:COLORS.main,
        height:height*0.05,
        justifyContent:'center',
        alignItems:'center',
        textAlignVertical:'center',
        textAlign:'center',
        fontSize: width*0.04,
        borderRadius:width*0.02,
        width:width*0.14
        // borderTopRightRadius:width*0.02,
        // borderBottomRightRadius:width*0.02
    },
    messegeItemContainer:{
        width:width*0.94,
        marginVertical:width*0.01,
    },
    myMessegeContainer:{
        minHeight:height*0.05,
        alignSelf:'flex-start',
        // backgroundColor:"rgba(143, 176, 2, 0.8)",
        backgroundColor:COLORS.text,
        borderBottomRightRadius:width*0.03,
        borderTopRightRadius:width*0.03,
        paddingHorizontal:width*0.02,
        minWidth:width*0.1,
        maxWidth:width*0.7,
        paddingVertical:width*0.01,
        paddingRight:width*0.1,
        justifyContent:'center',
        alignItems:'center',
    },
    otherMessegeContainer:{
        minHeight:height*0.05,
        alignSelf:'flex-end',
        backgroundColor:COLORS.textGray,
        borderBottomLeftRadius:width*0.03,
        borderTopLeftRadius:width*0.03,
        paddingHorizontal:width*0.02,
        minWidth:width*0.1,
        maxWidth:width*0.7,
        paddingVertical:width*0.01,
        paddingRight:width*0.1,
        justifyContent:'center',
        alignItems:'center',
    },
    myMessege:{
        color: COLORS.textBlack,
        textAlignVertical:'center',
        fontSize:width*0.04,
    },
    otherMessege:{
        color: COLORS.textBlack,
        textAlignVertical:'center',
        fontSize:width*0.04,
    },
    date:{
        position:'absolute', 
        bottom:width*0.028, 
        right:width*0.01, 
        fontSize:width*0.03, 
        color:"rgb(50,50,50)"
    }
})