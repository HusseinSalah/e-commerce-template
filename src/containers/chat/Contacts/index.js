import React from 'react';
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    Image
} from 'react-native';
import {styles} from './styles';
import {width, height, COLORS, IMAGES, ICONS} from '../../../common';
import Header from '../../../components/Header';
import Localization from '../../../language';

class Contacts extends React.Component{
    constructor(props){
        super();
        this.state={
            contacts:[
                {id:'1',name:'الحسين صلاح', image:IMAGES.avatar, lastMessege:'السلام عليكم ورحمة الله وبركاتة الكثير من الكلام يوجد هنا ', lastMessegeDate:'13:36'},
                {id:'2',name:'الحسين صلاح', image:IMAGES.avatar, lastMessege:'السلام عليكم ورحمة الله وبركاتة', lastMessegeDate:'13:36'},
                {id:'3',name:'الحسين صلاح', image:IMAGES.avatar, lastMessege:'السلام عليكم ورحمة الله وبركاتة', lastMessegeDate:'13:36'},
                {id:'4',name:'الحسين صلاح', image:IMAGES.avatar, lastMessege:'السلام عليكم ورحمة الله وبركاتة', lastMessegeDate:'13:36'},
                {id:'5',name:'الحسين صلاح', image:IMAGES.avatar, lastMessege:'السلام عليكم ورحمة الله وبركاتة', lastMessegeDate:'13:36'},
                {id:'6',name:'الحسين صلاح', image:IMAGES.avatar, lastMessege:'السلام عليكم ورحمة الله وبركاتة', lastMessegeDate:'13:36'},
                {id:'7',name:'الحسين صلاح', image:IMAGES.avatar, lastMessege:'السلام عليكم ورحمة الله وبركاتة', lastMessegeDate:'13:36'},
                {id:'8',name:'الحسين صلاح', image:IMAGES.avatar, lastMessege:'السلام عليكم ورحمة الله وبركاتة', lastMessegeDate:'13:36'},
                {id:'9',name:'الحسين صلاح', image:IMAGES.avatar, lastMessege:'السلام عليكم ورحمة الله وبركاتة', lastMessegeDate:'13:36'},
            ],

        }
    }

    static navigationOptions ={
        header: <Header title="المحادثات" />
    }

    _renderChatItem = ({item})=>{
        return(
            <TouchableOpacity style={styles.chatContainer} onPress={()=>this.props.navigation.navigate('ContactChat', {'OtherName':item.name})}>
                <Image source={item.image} style={styles.avatar}/>
                <View style={styles.middle}>
                    <Text style={styles.name}> {item.name} </Text>
                    <Text style={styles.messege}> {item.lastMessege.substr(0,32)}{item.lastMessege.length>32?'...':''} </Text>
                </View>
                <Text style={styles.date}> {item.lastMessegeDate} </Text>
            </TouchableOpacity>
        )
    }

    render(){
        return(
            <View style={styles.container}>
                {/* <Image source={IMAGES.chatsBG} style={{resizeMode:'stretch', height:height, width:width, opacity:0.5, position:'absolute'}}/> */}
                <FlatList
                    data={this.state.contacts}
                    renderItem={this._renderChatItem}
                    // style={{backgroundColor:'red'}}
                    keyExtractor={(item)=> item.id}
                    showsVerticalScrollIndicator={false}
                />
            </View>
        )
    }
}

export default Contacts;