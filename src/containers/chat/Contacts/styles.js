import{
    StyleSheet
} from 'react-native';
import { width, height, COLORS } from '../../../common';

export const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal:width*0.02,
        backgroundColor:COLORS.background,
    },
    chatContainer:{
        justifyContent:'space-between',
        alignItems:'center',
        height: height*0.12,
        width:width*0.96,
        backgroundColor:COLORS.text,
        flexDirection:'row',
        marginVertical:width*0.01,
        borderRadius:width*0.02,
        elevation:3,
    },
    avatar:{
        width:width*0.12,
        height:width*0.12,
        resizeMode:'contain',
        marginHorizontal:width*0.02,
        borderRadius:width*0.1,
    },
    middle:{
        width:width*0.64,
        // backgroundColor:COLORS.main
    },
    name:{
        fontSize:width*0.045,
        color:COLORS.textBlack,
        fontWeight:'bold',
        marginBottom:width*0.01
    },
    messege:{
        fontSize:width*0.04,
        color:COLORS.gray,
    },
    date:{
        fontSize:width*0.035,
        color: COLORS.gray,
        marginBottom:width*0.09,
        marginHorizontal:width*0.02,
    }
})