import {
    StyleSheet,
} from 'react-native'
import { COLORS, width, height } from '../../../common';

export const styles = StyleSheet.create({
    container:{
        alignItems:'center',
        flex: 1,
        backgroundColor:COLORS.white,
    },
    text:{
        color :'black',
        fontSize: 32,
    }
}) 