import React from 'react';
import {
    View,
    Text,
    ScrollView,
    StatusBar,
} from 'react-native';
import {styles} from './styles';
import HeaderBanner from '../../../components/HeaderBanner';
import CustomFlatList from '../../../components/CustomFlatList';
import CustomHorizontalFlatList from '../../../components/CustomHorizontalFlatList';
import { IMAGES, COLORS } from '../../../common';
import Header from '../../../components/Header';
import Localization from '../../../language';

class Home extends React.Component {
    constructor(props){
        super();
        this.state={
            bannerData:[
                {data:"data", image:IMAGES.homeBanner},
                {data:"data", image:IMAGES.homeBanner},
                {data:"data", image:IMAGES.homeBanner},
            ],
            subCategories:[
                {id:1, name:"category", image:IMAGES.category},
                {id:1, name:"category", image:IMAGES.category},
                {id:1, name:"category", image:IMAGES.category},
                {id:1, name:"category", image:IMAGES.category},
                {id:1, name:"category", image:IMAGES.category},
                {id:1, name:"category", image:IMAGES.category},
                {id:1, name:"category", image:IMAGES.category},
            ],
            products:[
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product},
            ]
        }
    }

    static navigationOptions = ({ navigation }) => {
        //return header with Custom View which will replace the original header 
        // console.warn(JSON.stringify(navigation))
        return {
          header: (<Header title={Localization.category}/>),
        };
    };

    render(){
        return(
            <ScrollView>
            <View style={styles.container}>
                <StatusBar backgroundColor={COLORS.main} barStyle="light-content" />
                <HeaderBanner bannerHeight={0.2} items={this.state.bannerData}/>
                <CustomHorizontalFlatList
                    title={Localization.SubCategories}
                    items={this.state.subCategories}    
                    onPress={()=>{this.props.navigation.navigate('Products'); console.warn("Products")}}
                />
                <CustomFlatList 
                    title={Localization.BestProductsinthisCategory}
                    bannerHeight={0.2}
                    items={this.state.products}
                    onPress={()=>{this.props.navigation.navigate('Product'); console.warn("Product")}}
                    />
                {/* <Text style={styles.text}> home screen </Text> */}
            </View>
            </ScrollView>
        )
    }
}

export default Home;