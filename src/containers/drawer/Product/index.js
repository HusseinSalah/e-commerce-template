import React from 'react';
import {
    View,
    Text,
    ScrollView,
    StatusBar,
    TouchableOpacity,
    FlatList
} from 'react-native';
import {styles} from './styles';
import HeaderBanner from '../../../components/HeaderBanner';
import CustomFlatList from '../../../components/CustomFlatList';
import { IMAGES, COLORS, width, height } from '../../../common';
import { Line } from '../../../components';
import Header from '../../../components/Header';
import { Rating } from 'react-native-elements';
import { HeaderText } from '../../../components/HeaderText';
import { Icon } from 'react-native-elements';
import Localization from '../../../language';

class Home extends React.Component {
    constructor(props){
        super();
        this.state={
            bannerData:[
                {data:"", image:IMAGES.productSlider},
                {data:"", image:IMAGES.productSlider1},
                {data:"", image:IMAGES.productSlider},
            ],
            products:[
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product2},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product},
                {id:1, name:"product", image:IMAGES.product1},
                {id:1, name:"product", image:IMAGES.product},
            ],
            reviews:[
                {name:"Hussein Salah", reviewText:"some good or bad feedback whatever we have to show to all the truth.", reviewRate:3.8, date:"25-1-2019"},
                {name:"Mohamed Elseid", reviewText:"some good or bad feedback whatever we have to show to all the truth.", reviewRate:4.2, date:"25-1-2019"},
                {name:"Hamed Ahmed", reviewText:"some good or bad feedback whatever we have to show to all the truth.", reviewRate:4.5, date:"25-1-2019"},
                {name:"Ghada Ahmed", reviewText:"some good or bad feedback whatever we have to show to all the truth.", reviewRate:3.5, date:"25-1-2019"},
            ]
        }
    }

    static navigationOptions = ({ navigation }) => {
        //return header with Custom View which will replace the original header 
        // console.warn(JSON.stringify(navigation))
        return {
          header: (<Header title={Localization.product}/>),
        };
    };

    _renderReview = ({item})=>{
        return(
            <View style={{backgroundColor:COLORS.text, paddingBottom:width*0.02}}>
                <View style={styles.reviewHead}>
                    <Text style={styles.reviewTextHead}> {item.name} </Text>
                    <View>
                        <Rating
                            imageSize={width*0.05}
                            style={{ paddingVertical: 5 }}
                            startingValue={item.reviewRate}
                            readonly
                            tintColor={COLORS.background}
                            
                        />
                        <Text style={styles.dateText}> {item.reviewRate} </Text>
                    </View>
                </View>
                <Text style={styles.reviewText}> {item.reviewText} </Text>
                <Text style={styles.dateText}> {item.date} </Text>
            </View>
        )
    }

    render(){
        return(
            <ScrollView>
            <View style={styles.container}>
                <StatusBar backgroundColor={COLORS.main} barStyle="light-content" />
                <HeaderBanner bannerHeight={0.4} items={this.state.bannerData}/>
                <View style={styles.subContainer}>
                    <View style={[styles.horCenter, {justifyContent:'space-between'}]}>
                        <Text style={styles.text}>Product name </Text>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Cart')} style={{backgroundColor:COLORS.main, flexDirection:'row', borderRadius:width*0.02, elevation:4, justifyContent:'center', alignItems:'center', padding:width*0.015}}>
                            <Text style={{color:COLORS.text, fontSize:width*0.035}}>  {Localization.addToCart} </Text>
                            <Icon name="cart-plus" type="font-awesome" color={COLORS.text} size={width*0.05}/>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.horCenter, {justifyContent:'space-between'}]}>
                        <View>
                            <Rating
                                imageSize={width*0.06}
                                style={{ paddingVertical: 10 }}
                                startingValue={4.5}
                                readonly
                            />
                            <Text> 315 review </Text>
                        </View>
                        <View style={[styles.horCenter, {width:width*0.3}]}>
                            <Text style={styles.currentPrice}> 150$ </Text>
                            <Text style={styles.previousPrice}> 220$ </Text>
                        </View>
                    </View>
                    <Line width={width*0.8}/>
                    <HeaderText style={{color:COLORS.text}} title={Localization.Description}/>
                    <View style={styles.backGround}>
                        <Text>some Description of the product some Description of the product some Description of the product some Description of the product some Description of the product  </Text>
                    </View>
                    <HeaderText style={{color:COLORS.text}} title={Localization.ProductDetails}/>
                    <View style={styles.backGround}>
                        <View style={styles.details}>
                            <View style={styles.left}>
                                <Text style={{color:COLORS.textBlack, fontSize:width*0.035}}> {Localization.producttitle} </Text>
                            </View>
                            <View style={styles.right}>
                                <Text style={{color:COLORS.textBlack, fontSize:width*0.035}}> Polo T-Shirt For Men </Text>
                            </View>
                        </View>
                        <Line width={width*0.78} color={COLORS.secondery}/>
                        
                        <View style={styles.details}>
                            <View style={styles.left}>
                                <Text style={{color:COLORS.textBlack, fontSize:width*0.035}}> {Localization.ItemEAN} </Text>
                            </View>
                            <View style={styles.right}>
                                <Text style={{color:COLORS.textBlack, fontSize:width*0.035}}> 8554744521445 </Text>
                            </View>
                        </View>
                        <Line width={width*0.78} color={COLORS.secondery}/>
                        
                        <View style={styles.details}>
                            <View style={styles.left}>
                                <Text style={{color:COLORS.textBlack, fontSize:width*0.035}}> {Localization.TargetedGroup} </Text>
                            </View>
                            <View style={styles.right}>
                                <Text style={{color:COLORS.textBlack, fontSize:width*0.035}}> Flowers </Text>
                            </View>
                        </View>
                        <Line width={width*0.78} color={COLORS.secondery}/>
                        
                        <View style={styles.details}>
                            <View style={styles.left}>
                                <Text style={{color:COLORS.textBlack, fontSize:width*0.035}}> {Localization.somedetails} </Text>
                            </View>
                            <View style={styles.right}>
                                <Text style={{color:COLORS.textBlack, fontSize:width*0.035}}> some details </Text>
                            </View>
                        </View>
                        <Line width={width*0.78} color={COLORS.secondery}/>

                    </View>
                    <HeaderText style={{color:COLORS.text}} title={Localization.PeopleReviews}/>
                    <View>
                        <FlatList 
                            data={this.state.reviews}
                            renderItem={this._renderReview}
                            showsVerticalScrollIndicator={false}
                        />
                    </View>
                </View>
            </View>
            </ScrollView>
        )
    }
}

export default Home;