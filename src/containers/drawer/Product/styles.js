import {
    StyleSheet,
} from 'react-native'
import { COLORS, width, height } from '../../../common';

export const styles = StyleSheet.create({
    container:{
        alignItems:'center',
        flex: 1,
        backgroundColor:COLORS.white,
    },
    subContainer:{
        alignItems:'flex-start',
        marginHorizontal:width*0.1,
        backgroundColor:COLORS.white,
        marginTop: height*0.01
    },
    text:{
        color :'black',
        fontSize: 32,
    },
    currentPrice:{
        color:COLORS.secondery,
        fontSize:width*0.055,
        textAlign:'center',
        textAlignVertical:'center',
        height:height*0.07,
        fontWeight:'bold',
    },
    previousPrice:{
        color:COLORS.textGray,
        fontSize:width*0.045,
        textAlign:'center',
        textAlignVertical:'bottom',
        height:height*0.07,
        fontWeight:'bold',
        textDecorationLine:'line-through',
    },
    horCenter:{
        flexDirection:'row', 
        justifyContent:'center', 
        alignItems:'center',
        width:width*0.8
    },
    backGround:{
        backgroundColor:COLORS.background,
        width:width*0.8,
        padding: width*0.01,
        borderRadius: width*0.02,
    },
    details:{
        justifyContent:'space-between',
        alignItems:'center',
        width:width*0.8,
        height:height*0.06,
        flexDirection:'row',
    },
    left:{
        justifyContent:'center',
        alignItems:'flex-start',
        width:width*0.3
    },
    right:{
        justifyContent:'center',
        alignItems:'flex-start',
        width:width*0.5
    },
    reviewHead:{
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor:COLORS.background,
        fontSize:width*0.35,
        flexDirection:'row',
    },
    dateText:{
        textAlign:'right',
        color:COLORS.textBlack,
        fontSize:width*0.03,
    },
    reviewTextHead:{
        fontSize:width*0.04,
        fontWeight:'bold',
        textAlign:'left',
    }
}) 