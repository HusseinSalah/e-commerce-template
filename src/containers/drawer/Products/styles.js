import {
    StyleSheet,
} from 'react-native'
import { COLORS } from '../../../common';

export const styles = StyleSheet.create({
    container:{
        alignItems:'center',
        flex: 1,
        backgroundColor:COLORS.background,
    },
    text:{
        color :'black',
        fontSize: 32,
    }
}) 