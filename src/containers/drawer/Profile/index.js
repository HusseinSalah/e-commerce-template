import React from 'react';
import {
    View,
    Text,
} from 'react-native';
import {styles} from './styles';
import { IMAGES, COLORS, width, height } from '../../../common';
import Header from '../../../components/Header';

class Home extends React.Component {

    static navigationOptions = ({ navigation }) => {
        //return header with Custom View which will replace the original header 
        // console.warn(JSON.stringify(navigation))
        return {
          header: (<Header title={navigation.state.routeName}/>),
        };
    };

    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.text}> Call us page </Text>
            </View>
        )
    }
}

export default Home;