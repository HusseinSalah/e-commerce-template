import React from 'react';
import {
    View,
    Text,
    FlatList,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import {styles} from './styles';
import { IMAGES, COLORS, ICONS, width, height } from '../../../../common';
import Header from '../../../../components/Header';
import {Line} from '../../../../components';
import { Icon } from 'react-native-elements';
import Localization from '../../../../language';


class Cart extends React.Component {

    constructor(){
        super();
        this.state = {
            products:[
                {name: "product product product", image:IMAGES.product1, price:50, quantity:2},
                {name: "product product product", image:IMAGES.product2, price:39, quantity:1},
                {name: "product product product", image:IMAGES.product, price:21, quantity:1},
                {name: "product product product", image:IMAGES.product1, price:89, quantity:3},
                {name: "product product product", image:IMAGES.product1, price:89, quantity:3},
                {name: "product product product", image:IMAGES.product1, price:89, quantity:3},
            ]
        }
    }

    _renderItemProduct = ({item})=>{
        return(
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('Product')} style={styles.productCartContainer}>
                <Image source={item.image} style={styles.productImage}/>
                <View style={styles.productPrice}>
                    <Text style={styles.productName}>{item.name.length>20?item.name.substring(0, 20)+".." : item.name.substring(0, 20)}</Text>
                    <View style={{flexDirection:'row'}}>
                        <Text style={styles.currentPrice}>EGP 150$ </Text>
                        <Text style={[styles.currentPrice, {color:COLORS.textGray, textDecorationLine:'line-through',}]}> 220$ </Text>
                    </View>
                    <Line width={width*0.5}/>
                    <View style={{flexDirection:'row', width:width*0.5, justifyContent:'space-between', alignItems:'center'}}>
                        <Icon onPress={()=>console.log('heart')} name="heart" type='font-awesome' size={width*0.06} color={COLORS.main} />
                        <TouchableOpacity style={{flexDirection:'row'}}>
                            <Image source={ICONS.remove} style={styles.removeIcon}/>
                            <Icon onPress={()=>console.log('trash')} name="trash" type='font-awesome' size={width*0.05} color={COLORS.secondery} />
                            <Text style={styles.remove}> {Localization.remove} </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{width:width*0.25, justifyContent:'center', alignItems:'center'}}>
                    <Icon onPress={()=>console.log('plus')} name="plus" type='font-awesome' size={width*0.05} color={COLORS.main} />
                    <Text style={styles.quantity}> 4 </Text>
                    <Icon onPress={()=>console.log('minus')} name="minus" type='font-awesome' size={width*0.05} color={COLORS.main} />
                </View>
            </TouchableOpacity>
        )
    }

    static navigationOptions = ({ navigation }) => {
        //return header with Custom View which will replace the original header 
        // console.warn(JSON.stringify(navigation))
        return {
          header: (<Header title={Localization.cart}/>),
        };
    };

    render(){
        return(
            <View style={styles.container}>
            <ScrollView>
                {/* <Header title={this.props.navigation.state.routeName}/> */}
                {/* <Header title="Cart"/> */}
                <View>
                    <FlatList 
                    data={this.state.products}
                    renderItem={this._renderItemProduct}
                    style={{zIndex:3, marginBottom:width*0.02}}
                    />
                </View>
                {this.state.products.length>0?
                <View>
                    <View style={styles.totalPrice}> 
                        <View style={{flexDirection:'row'}}>
                            <Text style={styles.left}>{Localization.subtotal} </Text>
                            <Text style={[styles.right]}> 220$ </Text>
                        </View>
                        {true?
                            <View style={{flexDirection:'row'}}>
                                <Text style={styles.left}>{Localization.shipping} </Text>
                                <Text style={[styles.right, {color:COLORS.secondery}]}> 20$ </Text>
                            </View>
                            :
                            null
                        }
                        <View style={{marginTop:width*0.03}}>
                            <Line width={width*0.8}/>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <Text style={[styles.left, {color:COLORS.main}]}> {Localization.total}</Text>
                            <Text style={[styles.right]}> 240$ </Text>
                        </View>
                    </View>
                    
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Delivery')} style={styles.completeOrder}>
                        <Text style={{fontSize:width*0.05, color:COLORS.text, fontWeight:'bold'}}> {Localization.completeOrder} </Text>
                    </TouchableOpacity>
                </View>
                :
                null}
            </ScrollView>
            </View>
        )
    }
}

export default Cart;