import {
    StyleSheet,
} from 'react-native'
import { COLORS, width, height } from '../../../../common';

export const styles = StyleSheet.create({
    container:{
        // alignItems:'center',
        backgroundColor:COLORS.background,
        // justifyContent:'center',
        height:height*0.89,
        paddingVertical:height*0.01
    },
    text:{
        color :'black',
        fontSize: 32,
    },
    productCartContainer:{
        width:width*0.94,
        height:height*0.14,
        justifyContent:'flex-start',
        alignItems:'center',
        flexDirection:'row',
        marginHorizontal:width*0.03,
        backgroundColor:COLORS.text,
        padding:width*0.02,
        borderWidth:width*0.003,
        marginVertical:height*0.006,
        borderColor:COLORS.main,
        borderRadius:width*0.02,
        elevation:2,
    },
    productImage:{
        width:width*0.2,
        height:height*0.1,
        resizeMode:'contain',
    },
    productPrice:{
        width:width*0.5,
        padding:width*0.05
    },
    productQuantity:{
        width:width*0.2,
    },
    currentPrice:{
        color:COLORS.secondery,
        fontSize:width*0.04,
        textAlign:'center',
        textAlignVertical:'center',
        fontWeight:'bold',
        padding:0
    },
    remove:{
        color:COLORS.secondery,
        fontSize:width*0.04,
        textAlign:'center',
        textAlignVertical:'center',
        // height:height*0.07,
        // fontWeight:,
    },
    previousPrice:{
        color:COLORS.textGray,
        fontSize:width*0.04,
        textAlign:'center',
        textAlignVertical:'bottom',
        height:height*0.07,
        fontWeight:'bold',
        textDecorationLine:'line-through',
    },
    horCenter:{
        flexDirection:'row', 
        justifyContent:'center', 
        alignItems:'center',
        width:width*0.8
    },
    quantity:{
        padding:width*0.01,
        color:COLORS.textGray,
        fontSize:width*0.035,
        borderWidth:width*0.002,
        borderColor:COLORS.background,
        marginVertical:width*0.01
    },
    productName:{
        color:COLORS.textBlack,
        fontSize:width*0.048,
        fontWeight:'bold',
        width:width*0.5,
        padding:0,
        marginBottom:height*0.01,
    },
    completeOrder:{
        width:width*0.94,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:width*0.05,
        backgroundColor:COLORS.main,
        height:height*0.06,
        marginHorizontal:width*0.03
    },
    totalPrice:{
        backgroundColor:COLORS.text,
        width:width*0.94,
        paddingHorizontal: width*0.02,
        paddingVertical: height*0.02,
        justifyContent:'flex-start',
        margin:width*0.03,
        alignItems:'center',
    },
    left:{
        color:COLORS.textBlack,
        fontSize:width*0.05,
        fontWeight:'bold',
        textAlign:'left',
        textAlignVertical:'center',
        width: width*0.45,
        // marginHorizontal:width*0.1
    },
    right:{
        color:COLORS.secondery,
        fontSize:width*0.05,
        fontWeight:'bold',
        textAlign:'right',
        textAlignVertical:'center',
        width: width*0.4,
        // backgroundColor:'red',
        // marginHorizontal:width*0.1
    },

}) 