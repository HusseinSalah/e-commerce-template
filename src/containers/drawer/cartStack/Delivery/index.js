import React from 'react';
import {
    View,
    Text,
    FlatList,
    Image,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import {styles} from './styles';
import { IMAGES, COLORS, ICONS, width, height } from '../../../../common';
import Header from '../../../../components/Header';
import {Line} from '../../../../components';
import { Icon } from 'react-native-elements';
import Localization from '../../../../language';
class Cart extends React.Component {

    constructor(){
        super();
        this.state = {
            products:[
                {name: "product product product", description:"product description is here with osme lol lol ", price:50, quantity:2},
                {name: "product product product", description:"product description is here with osme lol lol ", price:39, quantity:1},
                {name: "product product product", description:"product description is here with osme lol lol ", price:21, quantity:1},
                {name: "product product product", description:"product description is here with osme lol lol ", price:89, quantity:3},
            ]
        }
    }

    _renderItemAddress = ({item})=>{
        return(
            <View>
                <Text> element </Text>
            </View>
        )
    }

    static navigationOptions = ({ navigation }) => {
        //return header with Custom View which will replace the original header 
        // console.warn(JSON.stringify(navigation))
        return {
          header: (<Header title={navigation.state.routeName}/>),
        };
    };

    render(){
        return(
            <View style={styles.container}>
                <ScrollView>
                <View style={styles.subContainer}>
                    <Text style={[styles.text, {marginVertical: height*0.01, paddingHorizontal:0, color:COLORS.main}]}> {Localization.ADDRESSDETAILS} </Text>
                    <Text onPress={()=>console.warn("change address")} style={[styles.text, {marginVertical: height*0.01, color:COLORS.secondery}]}> {Localization.CHANGE} </Text>
                </View>
                <View style={styles.addressBackground}>
                    <Text style={[styles.text, styles.boldBlack]}> Hussein Salah </Text>
                    <Text style={styles.text}> مساكن الزهور </Text>
                    <Text style={styles.text}> Toukh </Text>
                    <Text style={styles.text}> Qualyobia </Text>
                    <Text style={styles.text}> Egypt </Text>
                    <Text style={styles.text}> +201023239809 </Text>
                </View>
                <Text style={[styles.text, {marginVertical: height*0.01, paddingHorizontal:0, color:COLORS.main}]}> {Localization.PACKAGEDETAILS} </Text>
                <View style={styles.addressBackground}>
                    {this.state.products.map((item)=>{
                        console.log(item);
                        return(
                            <View>
                                <View style={{justifyContent:'flex-start', flexDirection:'row', alignItems:'center', padding:width*0.03}}>
                                    <Text style={styles.text}>{item.quantity} X</Text>
                                    <Text style={styles.text}>{item.name} </Text>
                                </View>
                                <Text style={styles.text}>{item.description} </Text>
                                <Text style={styles.text}>EGP {item.price} </Text>
                                <View style={{width:width*0.94, marginVertical:width*0.03, justifyContent:'center', alignItems:'center'}}>
                                    <Line />
                                </View>
                            </View>
                        )
                    })}
                </View>

                <View>
                    <View style={styles.totalPrice}> 
                        <View style={{flexDirection:'row'}}>
                            <Text style={styles.left}>{Localization.subtotal} </Text>
                            <Text style={[styles.right]}> 220$ </Text>
                        </View>
                        {true?
                            <View style={{flexDirection:'row'}}>
                                <Text style={styles.left}>{Localization.shipping} </Text>
                                <Text style={[styles.right, {color:COLORS.secondery}]}> 20$ </Text>
                            </View>
                            :
                            null
                        }
                        <View style={{marginTop:width*0.03}}>
                            <Line width={width*0.8}/>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <Text style={[styles.left, {color:COLORS.main}]}>{Localization.total} </Text>
                            <Text style={[styles.right]}> 240$ </Text>
                        </View>
                    </View>
                </View>

                <TouchableOpacity onPress={()=>this.props.navigation.navigate('Payment')} style={styles.completeOrder}>
                    <Text style={{fontSize:width*0.05, color:COLORS.text, fontWeight:'bold'}}> {Localization.ProccedtoPayment} </Text>
                </TouchableOpacity>
                </ScrollView>
            </View>
        )
    }
}

export default Cart;