import React from 'react';
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
} from 'react-native';
import {styles} from './styles';
import { IMAGES, COLORS, ICONS, width, height } from '../../../../common';
import Header from '../../../../components/Header';
import {Line} from '../../../../components';
import { CheckBox } from 'react-native-elements';
import Localization from '../../../../language';

class Cart extends React.Component {

    constructor(){
        super();
        this.state = {
            addresses:[
                {name: "product product product", image:IMAGES.product1, price:50, quantity:2},
                {name: "product product product", image:IMAGES.product2, price:39, quantity:1},
                {name: "product product product", image:IMAGES.product, price:21, quantity:1},
                {name: "product product product", image:IMAGES.product1, price:89, quantity:3},
            ],
            checked:false
        }
    }

    _renderItemAddress = ({item})=>{
        return(
            <View>
                <Text> element </Text>
            </View>
        )
    }

    static navigationOptions = ({ navigation }) => {
        //return header with Custom View which will replace the original header 
        // console.warn(JSON.stringify(navigation))
        return {
          header: (<Header title={Localization.payment}/>),
        };
    };

    render(){
        return(
            <View style={styles.container}>
                {/* <Header title={this.props.navigation.state.routeName}/> */}
                <View>
                    <FlatList 
                    data={this.state.products}
                    renderItem={this._renderItemProduct}
                    style={{zIndex:3, marginBottom:width*0.02}}
                    />
                </View>
                
                <View style={styles.paymentView}>
                    <View style={styles.radioView}>
                        <CheckBox
                        title=' '
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checkedColor={COLORS.main}
                        checked={this.state.checked}
                        onPress={()=>this.setState({checked:!this.state.checked})}
                        containerStyle={{backgroundColor:COLORS.text, borderWidth:0, padding:0, margin:width*0.02}}
                        />
                    </View>
                    <View style={styles.textRadioView}>
                        <Text style={styles.text}> Cash on Delivery </Text>
                        {this.state.checked?
                            <Text style={[styles.text, {color:COLORS.textGray, marginVertical:width*0.01, fontSize:width*0.04}]}> 
                                if you select Cash On Deliver , you can pay for your package when our Delivery Associates bring it to your step or when you pick it up at one of our pickup Stations
                                1) We only accept Egyptian Pound
                                2)Because our Delivery Agents do not handle petty cash , we would appreciate that you have the exact amount for payment
                                3)Payment must be made before unsealing an electronic item such as a phone or laptop. Once the seal is broken the item can only be returned or rejected if it is damaged
                                ,defective or has missing parts. if you change your mind, an unsealed item con no longer be returned
                            </Text>
                        :null}
                    </View>
                </View>

                <View>
                    <View style={styles.totalPrice}> 
                        <View style={{flexDirection:'row'}}>
                            <Text style={styles.left}>{Localization.subtotal} </Text>
                            <Text style={[styles.right]}> 220$ </Text>
                        </View>
                        {true?
                            <View style={{flexDirection:'row'}}>
                                <Text style={styles.left}>{Localization.shipping} </Text>
                                <Text style={[styles.right, {color:COLORS.secondery}]}> 20$ </Text>
                            </View>
                            :
                            null
                        }
                        <View style={{marginTop:width*0.03}}>
                            <Line width={width*0.8}/>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <Text style={[styles.left, {color:COLORS.main}]}>{Localization.total} </Text>
                            <Text style={[styles.right]}> 240$ </Text>
                        </View>
                    </View>
                </View>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('Summary')}  style={styles.completeOrder}>
                    <Text style={{fontSize:width*0.05, color:COLORS.text, fontWeight:'bold'}}> {Localization.procceedToSummary} </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default Cart;