import {
    StyleSheet,
} from 'react-native'
import { COLORS, width, height } from '../../../../common';

export const styles = StyleSheet.create({
    container:{
        // alignItems:'center',
        backgroundColor:COLORS.background,
        // justifyContent:'center',
        height:height,
    },
    text:{
        color :COLORS.textBlack,
        fontSize: width*0.055,
        // fontWeight:'bold',
        width:width*0.8,
        padding:width*0.02,
        // textAlign:'left',
    },
    paymentView:{
        flexDirection:'row',
        backgroundColor:COLORS.text,
        marginHorizontal:width*0.02,
        padding:width*0.01,
    },
    radioView:{
        width:width*0.14,
    },
    textRadioView:{
        width:width*0.8,
    },
    completeOrder:{
        width:width*0.94,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:width*0.05,
        backgroundColor:COLORS.main,
        height:height*0.06,
        marginHorizontal:width*0.03
    },
    totalPrice:{
        backgroundColor:COLORS.text,
        width:width*0.94,
        paddingHorizontal: width*0.02,
        paddingVertical: height*0.02,
        justifyContent:'flex-start',
        margin:width*0.03,
        alignItems:'center',
    },
    left:{
        color:COLORS.textBlack,
        fontSize:width*0.05,
        fontWeight:'bold',
        textAlign:'left',
        textAlignVertical:'center',
        width: width*0.45,
        // marginHorizontal:width*0.1
    },
    right:{
        color:COLORS.secondery,
        fontSize:width*0.05,
        fontWeight:'bold',
        textAlign:'right',
        textAlignVertical:'center',
        width: width*0.4,
        // backgroundColor:'red',
        // marginHorizontal:width*0.1
    },

}) 