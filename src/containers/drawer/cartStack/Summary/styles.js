import {
    StyleSheet,
} from 'react-native'
import { COLORS, width, height } from '../../../../common';

export const styles = StyleSheet.create({
    container:{
        backgroundColor:COLORS.background,
        height:height*0.89,
        padding: width*0.03,
    },
    text:{
        color :COLORS.gray,
        fontSize: width*0.04,
        paddingHorizontal: width*0.02,
        textAlign: 'left',
        marginVertical:width*0.01
    },
    boldBlack:{
        // fontWeight:'bold',
        color: COLORS.textBlack,
    },
    addressBackground:{
        backgroundColor:COLORS.text,
        width:width*0.94,
        paddingVertical: height*0.01,
    },
    completeOrder:{
        width:width*0.94,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:width*0.05,
        backgroundColor:COLORS.main,
        height:height*0.06,
        marginVertical:width*0.03
    },
    subContainer:{
        justifyContent:'space-between',
        alignItems:'center',
        flexDirection:'row',
        marginTop:height*0.04
    },
    totalPrice:{
        backgroundColor:COLORS.text,
        width:width*0.94,
        paddingHorizontal: width*0.02,
        paddingVertical: height*0.02,
        justifyContent:'flex-start',
        marginTop:width*0.03,
        alignItems:'center',
    },
    left:{
        color:COLORS.textBlack,
        fontSize:width*0.05,
        fontWeight:'bold',
        textAlign:'left',
        textAlignVertical:'center',
        width: width*0.45,
        // marginHorizontal:width*0.1
    },
    right:{
        color:COLORS.secondery,
        fontSize:width*0.05,
        fontWeight:'bold',
        textAlign:'right',
        textAlignVertical:'center',
        width: width*0.4,
        // backgroundColor:'red',
        // marginHorizontal:width*0.1
    },

}) 