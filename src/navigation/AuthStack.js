import {createStackNavigator , createAppContainer } from 'react-navigation';
import Login from "../containers/AuthScreens/Login";
import Register from "../containers/AuthScreens/Register";
import ForgetPassword from "../containers/AuthScreens/ForgetPassword";
import{
    I18nManager
} from 'react-native'
const AuthStack = createStackNavigator({
    Login : {
        screen:Login,
        navigationOptions:{
            header: null,
        }
    },
    Register:{
        screen:Register,
        navigationOptions:{
            header:null,
        }
    },
    ForgetPassword: {
        screen: ForgetPassword,
        navigationOptions:{
            header:null,
        }   
    },
},{
    navigationOptions:{
        header: null
    }
})
export default createAppContainer(AuthStack);