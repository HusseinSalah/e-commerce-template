import React from 'react-native'; 
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Cart from '../containers/drawer/cartStack/Cart';
import Delivery from '../containers/drawer/cartStack/Delivery';
import Payment from '../containers/drawer/cartStack/Payment';
import Summary from '../containers/drawer/cartStack/Summary';

const CartStack = createStackNavigator({
    Cart        : Cart,
    Delivery    : Delivery,
    Payment     : Payment,
    Summary     : Summary,
},
{
    // navigationOptions:{
    //     header:null,
    // }
});

export default CartStack;


