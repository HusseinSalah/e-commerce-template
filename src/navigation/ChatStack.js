import {createStackNavigator , createAppContainer } from 'react-navigation';
import Contacts from "../containers/chat/Contacts";
import ContactChat from "../containers/chat/ContactChat";
import{
    I18nManager
} from 'react-native'
const ChatStack = createStackNavigator({
    Contacts,
    ContactChat,
})
export default createAppContainer(ChatStack);