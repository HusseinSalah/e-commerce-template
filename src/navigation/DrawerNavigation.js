import React from "react"
import { createDrawerNavigator, createAppContainer } from 'react-navigation'
import { ICONS, width, CONSTANTS } from "../common";
import Cart from "./CartStack";
import Profile from "../containers/drawer/Profile";
import Terms from "../containers/drawer/Terms";
import Callus from "../containers/drawer/Callus";
import Home from './HomeStack';
import Drawer from '../components/Drawer'; 
import{
    I18nManager
} from 'react-native'
const DrawerNavigation = createDrawerNavigator({
    Home:Home,
    Cart: Cart,
    Profile: Profile,
    Terms: Terms,
    Callus: Callus,
},{
    drawerPosition :!I18nManager.isRTL? "left":"right",
    drawerWidth : CONSTANTS.drawerWidth,
    contentComponent : Drawer,
    overlayColor:0.5,
})
export default createAppContainer(DrawerNavigation);