import React from 'react-native'; 
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Home from '../containers/drawer/Home';
import Category from '../containers/drawer/Category';
import Products from '../containers/drawer/Products';
import Product from '../containers/drawer/Product';

const HomeStack = createStackNavigator({
    Home        : Home,
    Category    : Category,
    Products    : Products,
    Product     : Product,
},
{
    // navigationOptions:{
    //     header:null,
    // }
});

export default HomeStack;


