

/*
    home slider
        [
            {image, title},
            ...
        ]
    top categories
        [
            {name, id, image},
            ...
        ]
    all categories
        [
            {name, id, image},
            ...
        ]
    offers prodcuts 
        [
            {name, id, image, price, offer},
            ...
        ]
    best products => rate or newest or most selling 
        [
            {name, id, image, price, offer(or null)},
            ...
        ]
    category products
        like best products
    product details
        {
            images:[
                {url},
                ...
            ],
            name,
            rate:{
                rateValue, #reviews
            },
            price,
            offer,
            description,
            productDetails:[
                {title, value},
                ...
            ],
            reviews:[
                {userName, reviewValue, reviewDate, reviewBody},
                ...
            ],
        }
    related products
        as best products but should be releated to the product id will be send with request
*/